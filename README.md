# Examples

> In this project, I aim to keep (or link to) working examples of different things. 


## Here

  - [Code Quality](code-quality/)
  - You can use emoji for [the names of stages](https://docs.gitlab.com/ee/ci/yaml/#stages) in GitLab CI. 


## Elsewhere

  - [DAST](https://gitlab.com/bcarranza/dast)
  - [Secret Detection](https://gitlab.com/bcarranza/detecting-secrets) in `bcarranza/detecting-secrets`. :unicorn:
