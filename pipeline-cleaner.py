#!/usr/bin/env python3
"""
Pipeline Cleaner || Keep your list of Pipelines nice and tidy.
"""

__author__ = "Brie Carranza"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
from logzero import logger
import requests
import json
import os

def naive_convert_project_name_to_project_id(project_name, auth_token_string):
    url = "https://gitlab.com/api/v4/projects"
    querystring = {"search": project_name}
    payload = ""
    headers = {
        'private-token': auth_token_string
    }
    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
    d = json.loads(response.text)
    project_id = str(d[0]['id'])
    found_project_name = str(d[0]['path_with_namespace'])
    logger.debug(project_id)
    return project_id, found_project_name


def get_web_url_from_project_name(project_name, auth_token_string):
    url = "https://gitlab.com/api/v4/projects"
    querystring = {"search": project_name}
    payload = ""
    headers = {
        'private-token': auth_token_string
    }
    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
    d = json.loads(response.text)
    web_url = str(d[0]['web_url']) + "/-/pipelines/"
    return web_url




def get_pipelines(project_name, auth_token_string):
    cleanup_required = False
    logger.debug("Collecting the project ID for " + project_name + ":")
    project_id, found_project_name = naive_convert_project_name_to_project_id(project_name, auth_token_string)
    logger.debug("I checked the API and found this path: " + found_project_name)
    # Construct the pipelines URL with the help of the project ID
    url = "https://gitlab.com/api/v4/projects/" + project_id + "/pipelines"
    payload = ""
    headers = {
        'private-token': auth_token_string,
        'content-type': "application/json"
        }
    response = requests.request("GET", url, data=payload, headers=headers)
    d = json.loads(response.text)
    list_of_statuses = list()
    for i in d:
        list_of_statuses.append(i['status'])
    list_of_statuses = list(set(list_of_statuses))
    logger.debug(list_of_statuses)
    number_canceled = 0
    number_failed = 0
    number_success = 0
    number_skipped = 0
    for i in d:
        if i['status'] == "skipped":
            number_skipped = number_skipped + 1
        if i['status'] == "canceled":
            number_canceled = number_canceled + 1
        if i['status'] == "failed":
            number_failed = number_failed + 1
        if i['status'] == "success":
            number_success = number_success + 1
    logger.info(" RESULTS ")
    logger.info("----------")
    if number_success > 0:
        logger.info("SUCCESSES: " + str(number_success))
    if number_canceled > 0:
        logger.info("CANCELED: " + str(number_canceled))
    if number_failed > 0:
        logger.info("FAILED: " + str(number_failed))
    if number_skipped > 0:
        logger.info("SKIPPED: " + str(number_skipped))
    if number_failed > 0 or number_canceled > 0:
        cleanup_required = True
    if cleanup_required:
        logger.warning("Cleanup is required.")
    if not cleanup_required:
        logger.info("Yay! No cleanup is required.")
    return cleanup_required, d, project_id


def do_the_cleanup(cleanup_required, d):
    if cleanup_required:
        logger.info("STARTING CLEANUP...")
    if not cleanup_required:
        print("""You really should not ever see this message, unless you 
                 are reading the source code.""")
    statuses_to_clean_up = list()
    statuses_to_clean_up = ["canceled", "failed"]
    list_of_failed_pipelines = list()
    for i in d:
        if i['status'] in statuses_to_clean_up:
            logger.info("I found a " + i['status'] + " pipeline with ID " + str(i['id']) + "...")
            list_of_failed_pipelines.append(i['id'])
    logger.info("TOTAL: I found " + str(len(list_of_failed_pipelines)) + " pipelines to cleanup for you.")
    return list_of_failed_pipelines

def tell_user_about_pipelines_page(project_name, auth_token_string):
    the_url = get_web_url_from_project_name(project_name, auth_token_string)
    logger.info("Check out the Pipelines page for this project online at: ")
    logger.info(the_url)

def delete_a_pipeline(project_id, pipeline_id, auth_token_string):
    logger.debug("In project " + str(project_id) + ", I will delete pipeline " + str(pipeline_id) + ".")
    url = "https://gitlab.com/api/v4/projects/" + str(project_id) + "/pipelines/" + str(pipeline_id)
    payload = ""
    headers = {
        'private-token': auth_token_string
        }
    response = requests.request("DELETE", url, data=payload, headers=headers)
    logger.info(response.text)


def delete_those_pipelines(project_id, list_of_failed_pipelines, auth_token_string, project_name):
    logger.debug("OK. I am going to list through the pipelines I was provided and delete each of them. ")
    logger.debug("This is in the context of the project with ID " + str(project_id))
    for pipeline in list_of_failed_pipelines:
        logger.debug("Deleting pipeline " + str(pipeline) + "...")
        delete_a_pipeline(project_id, pipeline, auth_token_string)
    logger.info("That marks the completion of the deletion.")


def get_auth_token_string():
    auth_token_string = str()
    auth_token_string = os.getenv('GITLAB_PERSONAL_ACCESS_TOKEN')
    return auth_token_string

def main(args):
    auth_token_string = get_auth_token_string()
    user_wants_report = str()
    user_wants_report = "n"
    """ Main entry point of the app """
    logger.info("Howdy! I am here to clean up some pipelines.")
    logger.info(args)
    logger.info("I am going to check the pipelines for " + args.repo_name + " now.")
    cleanup_required, d, project_id = get_pipelines(args.repo_name, auth_token_string)
    project_name = args.repo_name
    if cleanup_required:
        list_of_failed_pipelines = do_the_cleanup(cleanup_required, d)
        if args.mode == "auto":
            logger.warn("I am going to delete some stuff.")
            delete_those_pipelines(project_id, list_of_failed_pipelines, auth_token_string, project_name)
            tell_user_about_pipelines_page(project_name, auth_token_string)
        if args.mode == "prompt":
            logger.info("I will ask you first.")
            user_wants_clean_up = input("Would you like me to clean up (remove) the pipelines that I found? (y/n) ")
            if user_wants_clean_up == "y":
                delete_those_pipelines(project_id, list_of_failed_pipelines, auth_token_string, project_name)
                tell_user_about_pipelines_page(project_name, auth_token_string)
            if user_wants_clean_up == "n":
                logger.info("OK. I am not going to get rid of those pipelines.")
                user_wants_report = input("Would you like a list of links to the pipelines that I found but did not delete? (y/n) ")
            if user_wants_report == "y":
                print("The user wants a report, folks.")
                tell_user_about_pipelines_page(project_name, auth_token_string)
            else:
                tell_user_about_pipelines_page(project_name, auth_token_string)
                logger.info("Thanks for using the Pipeline Cleaner.")
    if not cleanup_required:
        tell_user_about_pipelines_page(project_name, auth_token_string)
        logger.info("All done. Have a great day!")


if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    # Required positional argument
    parser.add_argument("repo_name", help="Required. This is the name of the project that has pipelines that you want to cleanup. I will search through projects you have access to and use the first one that I find.")

    # Positional argument -- mode of operation
    parser.add_argument("mode", default="auto", help="Required. Use 'mode=auto' to have pipelines automatically deleted. Use 'mode=prompt' to be prompted when candidates for deletion have been identified. Default: mode=auto")

    # Optional argument flag which defaults to False
    parser.add_argument("-f", "--flag", action="store_true", default=False)


    # Optional argument which requires a parameter (eg. -d test)
    parser.add_argument("-n", "--name", action="store", dest="name")

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Verbosity (-v, -vv, etc)")

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version}) by {author}".format(version=__version__,author=__author__))

    args = parser.parse_args()
    main(args)

